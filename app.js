const express = require('express');
// const keys = require('./config/keys');
const stripe = require('stripe')('sk_test_ADBBOf7LN0wVjW9ZmdKaINoC');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const app = express();
const port = process.env.PORT || 5000;

// Handlebars Middleware
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Set Static Folder
app.use(express.static(`${__dirname}/public`));

//Index route
app.get('/', (req, res) => {
    res.render('index');
})

app.post('/charge', (req, res) => {
    const amount = 2500;
    console.log(req.body);
    stripe.customers.create({
        email: req.body.stripeEmail,
        source: req.body.stripeToken
    }).then((customer) =>
        stripe.charges.create({
            amount,
            description: 'Web Development EBook',
            currency: 'usd',
            customer: customer.id
        })).then(charge => res.render('success'));
    // res.send('TEST');
})


app.listen(port, function() {
    console.log('Server has started on ' + port);
})